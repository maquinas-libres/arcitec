/*

// IR lateral
difference () {
	union () {
		cube([70,8,17.5], center=true);
		translate([0,5,0])
		cube([85,3,18], center=true);
	}
	cube([67,9,15], center=true);
	rotate([90,0,0])
	cylinder (d=9.6, h=20,center=true);
}
*/

// Ultrasonico frente
difference () {
	cube([52,32,27], center=true);
	translate([0,-2,0])
	cube([46,32,21], center=true);


	translate([0,17,0])
	rotate([90,0,0])
	color ("red")
	HCSR04();
}

for (x =  [-10 : 4 : 10])
translate([0,15.5,1+x])
cube([46,1,1], center=true);

module HCSR04(extractHoles = false) {
        // Draw the sensors
        translate(v = [-13.5, 0, 7.5]) {
                cylinder(h = 13.8, r = 9, center = true);
        }
        translate(v = [13.5, 0, 7.5]) {
                cylinder(h = 13.8, r = 9, center = true);
        }

                difference() {
                        cube(size = [45,20,1.2], center = true);
                }
}


