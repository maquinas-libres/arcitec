Concepto
========

![imagen conceptual](img/concepto.svg)

Con el fin de interactuar con el publico, los qudinos fueron conectados a sensores de proximidad (ultrasónicos) y de presencia (PIR). Si el espectador viene de frente a  la instalación, es detectado, por el sensor ultrasónico de proximidad, que mide la distancia y cambia el color, de su led tricolor.

Con el fin de facilitar la acción del espectador y hacerlo intervenir en la obra, se coloco una alfombra con una escala de colores (rojo, naranja, amarillo, verde, azul, violeta, blanco), para que este, camine por la misma. Si pisa el color azul ,el qudino enciende el led en azul,si pasa a verde el led cambia a ese color,así sucesivamente.

En ausencia de espectadores el led se pone blanco. Si el espectador se acerca por la derecha de la instalación,se dispara el sensor infrarrojo volumétrico (PIR), el qudino acciona un servo, que indica, que color enciende el led. Mediante una flecha, muestra en una semicircunferencia de colores dicho color,además emite una escala de sonidos (Do,Re…), estableciendo una relación sinestesica color-sonido. En ausencia de espectadores el led también es blanco.


[Fotos y publicaciones de la obra](http://maquinaslibres.noblogs.org/tag/arcitec/)
