/*
 ---------------------------------------------
  Regla de colores
 ---------------------------------------------

  Este proyecto consiste en recrear una regla de
   colores que tiene un tamaño de 3 x 0,4m, en la
   medida que vas caminado por la regla el color
   el LED va cambiando de tonalidad, el color que
   en el momento estes pisando es el color que debe
   estar emitiendo el LED, para el desarrollo de ello
   hacemos uso de:

   -Arduino UNO
   -ShiftBrite
   -Sensor ultrasonico SRF05

   Arduino UNO
   -----------------
   Placa de control que toma la decisión de
   ajustar el color del LED según la distancia
   captada por el Sensor Ultrasónico SRF05.

   ShiftBrite
   -----------------
   Es un LED RGB (Red-Green-Blue) con la capacidad
   de emitir mil millones de colores distintos.

   Sensor ultrasónico SRF05
   -----------------
   Este sensor de ultrasonido nos permite medir
   la distancia a la cual se encuentra un objeto
   en un rango de 1cm a 4m.

 Cosas de Mecatrónica y Tienda de Robótica

*/

long colors[48] = {
  0xFFFFFF, 0xFF00C0, 0xFF00A0, 0xFF0080, 0xFF0060, 0xFF0040, 0xFF0020, 0xFF0000, //ROJO
  0xFF2000, 0xFF4000, 0xFF6000, 0xFF8000, 0xFFA000, 0xFFC000, 0xFFE000, 0xFFFF00, //AMARILLO
  0xE0FF00, 0xC0FF00, 0xA0FF00, 0x80FF00, 0x60FF00, 0x40FF00, 0x20FF00, 0x00FF00, //VERDE
  0x00FF20, 0x00FF40, 0x00FF60, 0x00FF80, 0x00FFA0, 0x00FFC0, 0x00FFE0, 0x00FFFF, //CIAN
  0x00E0FF, 0x00C0FF, 0x00A0FF, 0x0080FF, 0x0060FF, 0x0040FF, 0x0020FF, 0x0000FF, //AZUL
  0x2000FF, 0x4000FF, 0x6000FF, 0x8000FF, 0xA000FF, 0xC000FF, 0xE000FF, 0xFF00FF, //MAGENTA


};

//--------------------------------------------------
//Declara puertos de entradas y salidas y variables
//--------------------------------------------------

//Pines del Arduino conectados al ShiftBrite
int ledRojo = 11; //Declara Pin LED Rojo
int ledVerde = 10; //Declara Pin LED Verde
int ledAzul = 9; //Declara Pin LED Azul

int redPin = ledRojo;
int greenPin = ledVerde;
int bluePin = ledAzul;

// Pin CI

//Pines del Sensor ultrasónico SRF05
int ECHOPIN = 13;    // Pin para recibir el pulso de ECO
int TRIGPIN = 12;    // Pin para enviar el pulso de DISPARO



//Variables de distancia
int   distance = 0;           // Distancia capturada por el SRF05
int   distance1 = 0;
float preDistance = 200.0;    // Largo de la regla en cm menos 10cm  2702
int dn = 0;
long d[3];



//------------------------------------
//Funcion principal
//------------------------------------
void setup() // Se ejecuta cada vez que el Arduino se inicia
{
  Serial.begin(9600);

  // ------- colores como salidas
  pinMode(ledRojo, OUTPUT); //El LED Rojo como una salida
  pinMode(ledVerde, OUTPUT); //El LED Verde como una salida
  pinMode(ledAzul, OUTPUT); //El LED Azul como una salida
  //Configuración de pines para usar el ShiftBrite


  //Configuración de un pin como fuente de 5V
  pinMode(4, OUTPUT);    // Pin configurado como salida
  digitalWrite(4, HIGH); // Activado como ALTO para alimentar con 5V al SRF05


  //Configuración de pines para usar el SRF05
  pinMode(ECHOPIN, INPUT);   // Pin como entrada para recibir el ECO
  pinMode(TRIGPIN, OUTPUT);  // Pin como salida para emitir el DISPARO

}


//------------------------------------
//Funcion de enviar paquetes de datos al ShiftBrite
//------------------------------------


// Comunicación serial sincrona
//   ver referencia de la función en:
//   http://arduino.cc/es/Reference/ShiftOut






//------------------------------------
//Funcion cíclica
//------------------------------------
void loop() // Esta función se mantiene ejecutando
{ //  cuando este energizado el Arduino

  //-------------------------------------------------------
  //Rutina para calcular distancia con el SRF05
  //-------------------------------------------------------

  digitalWrite(TRIGPIN, LOW);   // Activa el pin de Disparo en bajo por 2uS
  delayMicroseconds(2);
  digitalWrite(TRIGPIN, HIGH);  // Activa el pin de Disparo en alto por 10uS
  delayMicroseconds(10);
  digitalWrite(TRIGPIN, LOW);   // Activa el pin de Disparo en bajo

  //Lectura del pulso
  // ver referencia: http://arduino.cc/es/Reference/PulseIn
  distance1 = pulseIn(ECHOPIN, HIGH);
  distance1 = floor(distance1 / 58 / 20) * 20; // Calcula la distancia del ancho del pulso

  d[dn] = distance1;
  dn ++;
  if (dn == 2) dn = 0;

  if  (distance1 == d[0] && distance1 == d[1]  )  {
    distance = distance1;
    Serial.println(distance);
  }



  //Paso 1
  if ((distance >= 0) && (distance < (preDistance * (2.0 / 7.0))))
  {
    //rojo
    setColor(colors[8]);
  } else if ( (distance < (preDistance * (3.0 / 7.0)))) {
    // anaranjado
    setColor(colors[10]);
  }
  else if ( (distance < (preDistance * (4.0 / 7.0))))
  {
    //amarillo 
    setColor(colors[16]);
  } else if ((distance < (preDistance * (5.0 / 7.0))))
  {
    //verde
    setColor(colors[24]);
  }  else if ((distance < (preDistance * (7.0 / 7.0)))) {
    //azul
    setColor(colors[32]);
  } else if ((distance < (preDistance * 1.5) ))
  {
    //violeta
    setColor(colors[40]);
  } else {
 
    // blanco
    setColor(colors[0]);
  }
  // retardo
  delay(1000 / 48);
}


void setColor(long rgb) {
  int red = rgb >> 16;
//  Serial.println("red:");
//  Serial.println(red);
  int green = (rgb >> 8) & 0xFF;
  int blue = rgb & 0xFF;
  analogWrite(redPin, 255 - red);
  analogWrite(greenPin, 255 - green);
  //Serial.println(green);
  analogWrite(bluePin, 255 - blue);
  //Serial.println(blue);
}
